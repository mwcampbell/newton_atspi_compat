// Copyright 2024 GNOME Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0 (found in
// the LICENSE-APACHE file) or the MIT license (found in
// the LICENSE-MIT file), at your option.

use accesskit::{ActionHandler, ActionRequest, NodeId, TreeUpdate};
use accesskit_atspi_common::{AdapterCallback, AppContext, InterfaceSet};
use newton_consumer::{
    Consumer as LowLevelConsumer, EventHandler as LowLevelEventHandler, GlobalSurfaceHandle,
    SurfaceHandle,
};
use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
};

pub use accesskit_atspi_common::{
    simplified, Adapter, CoordType, Error, Event, Granularity, Layer, NodeIdOrRoot, ObjectEvent,
    PlatformNode, PlatformRoot, Property, Rect, Result, Role, State, StateSet, WindowEvent,
};

pub trait EventHandler {
    fn enqueue_event(&self, adapter: &Adapter, event: Event);
    fn handle_pointer_moved_event(&self, adapter: &Adapter, surface_x: f64, surface_y: f64);
    fn handle_key_event(&self, released: bool, state: u32, keysym: u32, unichar: u32, keycode: u16);
    fn handle_events(&self);
}

struct MyActionHandler {
    surface: GlobalSurfaceHandle,
}

impl ActionHandler for MyActionHandler {
    fn do_action(&mut self, request: ActionRequest) {
        self.surface.send_action_request(request);
    }
}

struct MyAdapterCallback {
    event_handler: Arc<dyn EventHandler + Send + Sync>,
}

impl AdapterCallback for MyAdapterCallback {
    fn register_interfaces(&self, _adapter: &Adapter, _id: NodeId, _interfaces: InterfaceSet) {}

    fn unregister_interfaces(&self, _adapter: &Adapter, _id: NodeId, _interfaces: InterfaceSet) {}

    fn emit_event(&self, adapter: &Adapter, event: Event) {
        self.event_handler.enqueue_event(adapter, event);
    }
}

struct MyLowLevelHandler {
    // TODO: the consumer protocol should give us a way of distinguishing
    // between applications, so we can have multiple app contexts.
    app_context: Arc<RwLock<AppContext>>,
    event_handler: Arc<dyn EventHandler + Send + Sync>,
    adapters: HashMap<u32, Adapter>,
    pending_pointer_moves: HashMap<u32, (f64, f64)>,
}

impl MyLowLevelHandler {
    fn new(event_handler: Box<dyn EventHandler + Send + Sync>) -> Self {
        Self {
            app_context: AppContext::new(),
            event_handler: event_handler.into(),
            adapters: HashMap::new(),
            pending_pointer_moves: HashMap::new(),
        }
    }

    fn handle_pointer_move(&self, adapter: &Adapter, surface_x: f64, surface_y: f64) {
        self.event_handler
            .handle_pointer_moved_event(adapter, surface_x, surface_y);
    }
}

impl LowLevelEventHandler for MyLowLevelHandler {
    fn surface_removed(&mut self, id: u32) {
        self.adapters.remove(&id);
    }

    fn focus_gained(&mut self, surface: &mut SurfaceHandle) {
        if let Some(adapter) = self.adapters.get_mut(&surface.id()) {
            adapter.update_window_focus_state(true);
            self.event_handler.handle_events();
        } else {
            surface.start_updates();
        }
    }

    fn focus_lost(&mut self, surface: &mut SurfaceHandle) {
        if let Some(adapter) = self.adapters.get_mut(&surface.id()) {
            adapter.update_window_focus_state(false);
            self.event_handler.handle_events();
        }
    }

    fn pointer_moved(&mut self, surface: &mut SurfaceHandle, surface_x: f64, surface_y: f64) {
        if let Some(adapter) = self.adapters.get(&surface.id()) {
            self.handle_pointer_move(adapter, surface_x, surface_y);
        } else {
            surface.start_updates();
            self.pending_pointer_moves
                .insert(surface.id(), (surface_x, surface_y));
        }
    }

    fn update(&mut self, surface: &mut SurfaceHandle, update: TreeUpdate) {
        if let Some(adapter) = self.adapters.get_mut(&surface.id()) {
            adapter.update(update);
            self.event_handler.handle_events();
        } else {
            let mut adapter = Adapter::new(
                &self.app_context,
                MyAdapterCallback {
                    event_handler: Arc::clone(&self.event_handler),
                },
                update,
                false,
                Default::default(),
                MyActionHandler {
                    surface: surface.global(),
                },
            );
            if surface.has_focus() {
                adapter.update_window_focus_state(true);
                self.event_handler.handle_events();
            }
            if let Some((surface_x, surface_y)) = self.pending_pointer_moves.remove(&surface.id()) {
                self.handle_pointer_move(&adapter, surface_x, surface_y);
            }
            self.adapters.insert(surface.id(), adapter);
        }
    }

    fn key_event(&mut self, released: bool, state: u32, keysym: u32, unichar: u32, keycode: u16) {
        self.event_handler
            .handle_key_event(released, state, keysym, unichar, keycode);
    }
}

pub struct Consumer {
    lowlevel: LowLevelConsumer,
}

impl Consumer {
    pub fn new(event_handler: Box<dyn EventHandler + Send + Sync>) -> Self {
        let lowlevel_handler = MyLowLevelHandler::new(event_handler);
        let lowlevel = LowLevelConsumer::new(Box::new(lowlevel_handler));
        Self { lowlevel }
    }

    pub fn grab_keyboard(&self) {
        self.lowlevel.grab_keyboard();
    }

    pub fn ungrab_keyboard(&self) {
        self.lowlevel.ungrab_keyboard();
    }

    pub fn set_key_grabs(&self, modifiers: &[u32], keystrokes: &[(u32, u32)]) {
        self.lowlevel.set_key_grabs(modifiers, keystrokes);
    }
}
